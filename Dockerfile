FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/superset /app/pkg
WORKDIR /app/code

ENV PYTHONPATH=/app/code/superset:/app/pkg

RUN virtualenv -p /usr/bin/python3.10 /app/code/venv
ENV PATH=/app/code/venv/bin:/app/code/superset/bin:$PATH

RUN source /app/code/venv/bin/activate
RUN pip install --upgrade setuptools pip

# renovate: datasource=pypi depName=apache-superset versioning=pep440
ARG SUPERSET_VERSION=4.1.1

# Snowflake connector won't load without this - https://github.com/snowflakedb/snowflake-connector-python/issues/2108
ENV SNOWFLAKE_HOME=/run/snowflake-home

# prophet - https://github.com/apache/superset/blob/master/requirements/development.txt#L184
# to check if DBs got installed in a virtualenv console - import sqlalchemy.dialects ; sqlalchemy.dialects.__all__
RUN pip install -t /app/code/superset 'python-ldap>=3.4.4' 'Authlib>=1.3.0' 'Pillow>=10.0.1, <11' \
    # database - https://github.com/apache/superset/blob/master/docs/docs/configuration/databases.mdx and https://github.com/apache/superset/blob/master/pyproject.toml
    'psycopg2-binary==2.9.10' 'mysqlclient>=2.1.0, <3' 'pymssql>=2.2.8, <3' 'clickhouse-connect>=0.5.14, <1.0' 'snowflake-sqlalchemy>=1.2.4, <2' 'prophet>=1.1.5, <2' 'mysql-connector-python==8.4.0' \
    # bigquery has more than 1 dep
    'pandas-gbq>=0.19.1' 'sqlalchemy-bigquery>=1.6.1' 'google-cloud-bigquery>=3.10.0' \
    apache-superset==$SUPERSET_VERSION && \
    rm -rf /root/.pip/cache

# the superset cli expects this directory (but doesn't actually use it)
RUN ln -sf /run/superset/cli /home/cloudron/.superset && ln -sf /run/superset/cli /root/.superset

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/superset/supervisord.log /var/log/supervisor/supervisord.log

COPY env.sh superset_config.py cloudron_security_manager.py start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
