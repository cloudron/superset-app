#!/bin/bash

set -eu

mkdir -p /run/superset/cli /app/data/home /app/data/config /run/snowflake-home

if [[ ! -f /app/data/config/config_user.py ]]; then
    echo "=> First run"
    echo -e "# Add custom supervisor configuration here\n# https://superset.apache.org/docs/installation/configuring-superset\n" > /app/data/config/config_user.py
fi

if [[ ! -f /app/data/.secret_key ]]; then
    echo "=> Generating secret key"
    openssl rand -base64 42 > /app/data/.secret_key
fi

source /app/pkg/env.sh
source /app/code/venv/bin/activate

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data /run/superset /run/snowflake-home

# https://superset.apache.org/docs/installation/upgrading-superset#updating-superset-manually
echo "=> Running migrations"
gosu cloudron:cloudron superset db upgrade
echo "=> Setting up roles and perms"
gosu cloudron:cloudron superset init

# delete stale celery pids
rm -f /run/superset/*pid

echo "=> Starting Superset"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Superset
