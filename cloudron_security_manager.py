import logging
from superset.security import SupersetSecurityManager

class CloudronSecurityManager(SupersetSecurityManager):

    def oauth_user_info(self, provider, response=None):
        logging.debug("Oauth2 provider: {0}.".format(provider))
        if provider == 'Cloudron':
            me = self.appbuilder.sm.oauth_remotes[provider].get('me')
            data = me.json()
            logging.debug("user_data: {0}".format(data))
            return {
                'name' : data.get("name"),
                'email' : data.get("email"),
                'id' : data.get("sub"),
                'username' : data.get("sub"),
                'first_name': data.get("given_name"),
                'last_name': data.get("family_name")
                }

