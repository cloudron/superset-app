#!/bin/bash

export SECRET_KEY=$(</app/data/.secret_key)
export SUPERSET_HOME=/app/data/home

export SUPERSET_ENV=production

# default flask env when we run superset from CLI
export FLASK_DEBUG=False
export FLASK_APP="superset.app:create_app()"

