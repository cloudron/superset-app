[0.1.0]
* Initial version for Apache Superset

[0.1.1]
* Update demo data setup

[1.0.0]
* Add MS SQL driver

[1.0.1]
* Add Clickhouse adapter

[1.0.2]
* Add Big Query adapter

[1.0.3]
* Various packaging cleanups

[1.1.0]
* Update Superset to 2.1.0
* [Full changelog](https://github.com/apache/superset/releases/tag/2.1.0)
* fix(sqllab): SqlJsonExecutionContext.query null pointer by @serenajiang in #16997
* chore: removing use of supersetTheme in favor of ThemeProvider by @rusackas in #17000
* chore: no direct use of supersetTheme (or bad LESS vars) in SqlEditor by @rusackas in #16999
* fix: letter format of sort chart in dashboard edit by @jinghua-qa in #17003
* fix: rolling and cum operator on multiple series by @zhaoyongjie in #16945
* refactor(filterset): improve response body by @ofekisr in #16974
* chore: Select component refactoring - SelectAsyncControl - Iteration 5 by @geido in #16609
* chore: bump superset-ui 0.18.10 by @zhaoyongjie in #17009
* fix(dashboard): Race condition when setting activeTabs with nested tabs by @kgabryje in #17007
* fix: Strict check to allow null values in the SelectControl component by @geido in #17008

[1.2.0]
* Various packaging cleanups

[1.2.1]
* Update Cloudron base image to 4.2.0
* Update Superset to 3.0.0
* [Full changelog](https://github.com/apache/superset/releases/tag/3.0.0)

[1.3.0]
* Migrate to OIDC authentication

[1.3.1]
* Update Superset to 3.0.1
* [Full changelog](https://github.com/apache/superset/releases/tag/3.0.1)

[1.3.2]
* Cleanup stale pids on startup

[1.3.3]
* Update Supserset to 3.0.2
* [Full changelog](https://github.com/apache/superset/releases/tag/3.0.2)

[1.4.0]
* Add snowflake driver

[1.4.1]
* Update Supserset to 3.0.3
* [Full changelog](https://github.com/apache/superset/releases/tag/3.0.3)

[1.5.0]
* Update Superset to 3.1.0
* [Full changelog](https://github.com/apache/superset/blob/master/RELEASING/release-notes-3-1/README.md)

[1.5.1]
* Update Superset to 3.1.1
* [Full changelog](https://github.com/apache/superset/blob/3.1.1/CHANGELOG.md#311-fri-feb-9-214933-2024-0100)

[1.5.2]
* Update Superset to 3.1.2
* [Full changelog](https://github.com/apache/superset/blob/3.1.2/CHANGELOG.md#312-thu-mar-28-113200-2024--0300)
* #27706 fix: Select onChange is fired when the same item is selected in single mode (@michael-s-molina)
* #27744 fix: reduce alert error to warning (@eschutho)
* #27644 fix: Provide more inclusive error handling for saved queries (@john-bodley)
* #27646 fix: Leverage actual database for rendering Jinjarized SQL (@john-bodley)
* #27636 fix(sqllab): unable to remove table (@justinpark)

[1.5.3]
* Install prophet for predective analysis to work

[1.6.0]
* Update Superset to 4.0.0
* [Full changelog](https://github.com/apache/superset/blob/4.0.0/CHANGELOG/4.0.0.md)

[1.6.1]
* Update Superset to 4.0.1
* [Full changelog](https://github.com/apache/superset/blob/4.0.0/CHANGELOG/4.0.1.md)
* Fix installation of BigQuery connector

[1.6.2]
* Add mysql-connector-python package

[1.6.3]
* Update Superset to 4.0.2
* [Full changelog](https://github.com/apache/superset/releases/tag/4.0.2)

[1.6.4]
* Fix warnings
* Fix redis configuration

[1.7.0]
* Update apache-superset to 4.1.0
* [Full Changelog](https://github.com/apache/superset/releases/tag/4.1.0)

[1.7.1]
* Update apache-superset to 4.1.1
* [Full Changelog](https://github.com/apache/superset/releases/tag/4.1.1)

[1.7.2]
* Fix Snowflake Connector

[1.8.0]
* set HOME

