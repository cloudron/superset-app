# https://github.com/apache/superset/blob/306fb22021f84e7dd527c88c00be1f8e7cca0d8c/docker/pythonpath_dev/superset_config.py

import os
import sys
from cachelib.file import FileSystemCache
from celery.schedules import crontab
from flask_appbuilder.security.manager import AUTH_DB, AUTH_OAUTH

LOG_LEVEL = "INFO"

SECRET_KEY = os.environ["SECRET_KEY"]

SQLALCHEMY_DATABASE_URI = "%s://%s:%s@%s:%s/%s" % (
    "postgresql",
    os.environ["CLOUDRON_POSTGRESQL_USERNAME"],
    os.environ["CLOUDRON_POSTGRESQL_PASSWORD"],
    os.environ["CLOUDRON_POSTGRESQL_HOST"],
    os.environ["CLOUDRON_POSTGRESQL_PORT"],
    os.environ["CLOUDRON_POSTGRESQL_DATABASE"]
)

REDIS_HOST = os.environ["CLOUDRON_REDIS_HOST"]
REDIS_PORT = os.environ["CLOUDRON_REDIS_PORT"]
REDIS_CELERY_DB = "0"
REDIS_RESULTS_DB = "1"

# A storage location conforming to the scheme in storage-scheme. See the limits
# library for allowed values: https://limits.readthedocs.io/en/stable/storage.html
RATELIMIT_STORAGE_URI = f"redis://{REDIS_HOST}:{REDIS_PORT}"
# A callable that returns the unique identity of the current request.
# RATELIMIT_REQUEST_IDENTIFIER = flask.Request.endpoint

# suppress CSRF warnings
#WTF_CSRF_ENABLED = False
#TALISMAN_ENABLED = False

RESULTS_BACKEND = FileSystemCache("/app/data/home")

CACHE_CONFIG = {
    "CACHE_TYPE": "RedisCache",
    "CACHE_DEFAULT_TIMEOUT": 86400,
    "CACHE_KEY_PREFIX": "superset_filter_cache",
    "CACHE_REDIS_URL": f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_RESULTS_DB}"
}
EXPLORE_FORM_DATA_CACHE_CONFIG = FILTER_STATE_CACHE_CONFIG = DATA_CACHE_CONFIG = CACHE_CONFIG

class CeleryConfig(object):
    broker_connection_retry_on_startup = True
    broker_url = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_CELERY_DB}"
    imports = (
        "superset.sql_lab",
        "superset.tasks.scheduler",
    )
    result_backend = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_CELERY_DB}"
    worker_prefetch_multiplier = 10
    task_acks_late = True
    task_annotations = {
        "sql_lab.get_sql_results": {
            "rate_limit": "100/s",
        },
    },
    beat_schedule = {
        "reports.scheduler": {
            "task": "reports.scheduler",
            "schedule": crontab(minute="*", hour="*"),
        },
        "reports.prune_log": {
            "task": "reports.prune_log",
            "schedule": crontab(minute=10, hour=0),
        },
    }

CELERY_CONFIG = CeleryConfig

FEATURE_FLAGS = {"ALERT_REPORTS": True}
ALERT_REPORTS_NOTIFICATION_DRY_RUN = True

WEBDRIVER_BASEURL = "http://0.0.0.0:8088/"
WEBDRIVER_BASEURL_USER_FRIENDLY = WEBDRIVER_BASEURL

SQLLAB_CTAS_NO_LIMIT = True

# CLOUDRON_OIDC_PROVIDER_NAME is not supported
CLOUDRON_OIDC_ISSUER = os.environ["CLOUDRON_OIDC_ISSUER"]
CLOUDRON_OIDC_TOKEN_ENDPOINT = os.environ["CLOUDRON_OIDC_TOKEN_ENDPOINT"]
CLOUDRON_OIDC_AUTH_ENDPOINT = os.environ["CLOUDRON_OIDC_AUTH_ENDPOINT"]

# http location to https fix
ENABLE_PROXY_FIX = True

from cloudron_security_manager import CloudronSecurityManager
CUSTOM_SECURITY_MANAGER = CloudronSecurityManager

AUTH_TYPE = AUTH_OAUTH
AUTH_ROLE_ADMIN = 'Admin'
AUTH_ROLE_PUBLIC = 'Public'
AUTH_USER_REGISTRATION = True
AUTH_USER_REGISTRATION_ROLE = "Admin"

OAUTH_PROVIDERS = [
    {
        'name':'Cloudron',
        'token_key':'access_token',
        'icon':'fa-address-card',
        'remote_app': {
            'client_id': os.environ["CLOUDRON_OIDC_CLIENT_ID"],
            'client_secret': os.environ["CLOUDRON_OIDC_CLIENT_SECRET"],
            'client_kwargs': {'scope': 'openid email profile'},
            'server_metadata_url': f"{CLOUDRON_OIDC_ISSUER}/.well-known/openid-configuration",
            'api_base_url': f"{CLOUDRON_OIDC_ISSUER}/",
            'access_token_url': f"{CLOUDRON_OIDC_TOKEN_ENDPOINT}",
            'authorize_url': f"{CLOUDRON_OIDC_AUTH_ENDPOINT}"
        }
    }
]

# Import user local settings
sys.path.append('/app/data/config')
import config_user
from config_user import *  # noqa

