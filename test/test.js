#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    let athenticated_by_oidc = false;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/logout/`);

        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/login`);

        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Settings"]')), TIMEOUT);
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath('//a[contains(., "Sign In with Cloudron")]')), TIMEOUT);
        await browser.findElement(By.xpath('//a[contains(., "Sign In with Cloudron")]')).click();
        await browser.sleep(2000);

        if (!athenticated_by_oidc) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);

            athenticated_by_oidc = true;
        }

        await browser.wait(until.elementLocated(By.xpath('//span[text()="Settings"]')), TIMEOUT);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/superset/welcome/');
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(@class, "welcome-header")] | //span[text()="Settings"]')), TIMEOUT);
    }

    async function checkDemoData() {
        await browser.get('https://' + app.fqdn + '/dashboard/list/');
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="header" and contains(text(), "Dashboard")]')), TIMEOUT);
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//table//tr[@class="table-row"]')), TIMEOUT);
    }

    async function createDashboard() {
        await browser.get('https://' + app.fqdn + '/superset/welcome/');
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(text(), "Dashboard")]')).click();
        await browser.sleep(2000);

        let title_field = browser.findElement(By.xpath('//input[@aria-label="Dashboard title"]'));
        await title_field.click();
        await title_field.sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.sleep(2000);
        await title_field.sendKeys('TestDashboard');
        await title_field.sendKeys(Key.RETURN);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(., "Save")]')).click();
        await browser.sleep(5000);
    }

    async function checkDashboard() {
        await browser.get('https://' + app.fqdn + '/dashboard/list/');
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="header" and contains(text(), "Dashboard")]')), TIMEOUT);
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//td[contains(@class, "table-cell")]//a[contains(., "TestDashboard")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('can create Dashboard', createDashboard);
    it('check Dashboard', checkDashboard);

    it('can LOAD DEMO DATA', async function() {
        console.log('Loading demo data, this can take a long time');
        execSync(`cloudron exec --app ${app.id} -- bash -c "source /app/pkg/env.sh && superset fab create-admin --username admin --password changeme --firstname admin --lastname admin --email support@cloudron.io && superset load_examples 2>&1"`);
    });

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('check DEMO DATA', checkDemoData);
    it('check Dashboard', checkDashboard);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can get the main page', getMainPage);

    it('check DEMO DATA', checkDemoData);
    it('check Dashboard', checkDashboard);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await sleep(15000);
    });
    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('check DEMO DATA', checkDemoData);
    it('check Dashboard', checkDashboard);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id org.apache.superset.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('can create Dashboard', createDashboard);
    it('check Dashboard', checkDashboard);

    it('can LOAD DEMO DATA', async function() {
        console.log('Loading demo data, this can take a long time');
        execSync(`cloudron exec --app ${app.id} -- bash -c "source /app/pkg/env.sh && superset fab create-admin --username admin --password changeme --firstname admin --lastname admin --email support@cloudron.io && superset load_examples 2>&1"`);
    });
    it('check DEMO DATA', checkDemoData);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', loginOIDC.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('check DEMO DATA', checkDemoData);
    it('check Dashboard', checkDashboard);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

